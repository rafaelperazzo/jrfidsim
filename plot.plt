set encoding utf8 #encoding
unset format
unset label
set key left top
set xlabel "Number of tags"
set ylabel "System efficiency"
set autoscale
set mxtics
set mytics
set grid xtics ytics lt 2 lw 0.5 lc rgb "#BEBEBE"
set style line 1 lc rgb 'black' lt 1 lw 3  pt 7 ps 0
set style line 2 lc rgb 'blue' lt 2 lw 3 pt 5 ps 0
set style line 3 lc rgb 'red' lt 1 lw 2 pt 9 ps 0
set style line 4 lc rgb 'orange' lt 2 lw 2 pt 4 ps 0
set style line 5 lc rgb 'yellow' lt 9 lw 2 pt 6 ps 0
set style line 6 lc rgb 'green' lt 9 lw 2 pt 6 ps 0
set terminal postscript eps
set output "plot.sef.eps"
set border linewidth 3
set key default
set label 1 "Confidence Interval (CI) 90%" at 300,0.48
set label 2 "1000 iterations" at 300,0.47
set format y "%.2f"
set xrange [0:15200]
set yrange [0.20:0.50]
set xtics 0,200,15200 rotate by 45 offset -0.8,-2
set ytics 0.02
plot '01_SEF.MOTA.4.15000.txt' using 1:2:3:4 with linespoint ls 1 title "Our Algorithm", '01_SEF.MOTA.4.15000.txt' using 1:2:3:4 with errorbars ls 1 notitle,'01_SEF.C1G2.4.15000.txt' using 1:2:3:4 with linespoint ls 2 title "C1G2", '01_SEF.C1G2.4.15000.txt' using 1:2:3:4 with errorbars ls 2 notitle,'01_SEF.LOWER.128.15000.txt' using 1:2:3:4 with linespoint ls 3 title "LOWER", '01_SEF.LOWER.128.15000.txt' using 1:2:3:4 with errorbars ls 3 notitle,'01_SEF.SCHOUTE.128.15000.txt' using 1:2:3:4 with linespoint ls 4 title "SCHOUTE", '01_SEF.SCHOUTE.128.15000.txt' using 1:2:3:4 with errorbars ls 4 notitle,'01_SEF.NEDFSA.4.15000.txt' using 1:2:3:4 with linespoint ls 6 title "NEDFSA", '01_SEF.NEDFSA.4.15000.txt' using 1:2:3:4 with errorbars ls 6 notitle,'01_SEF.EOMLEE.128.15000.txt' using 1:2:3:4 with linespoint ls 5 title "EOMLEE", '01_SEF.EOMLEE.128.15000.txt' using 1:2:3:4 with errorbars ls 5 notitle
unset format
unset label
set key default
set key left top
set xlabel "Number of tags"
set ylabel "Mean identification time (slots)"
set autoscale
set mxtics
set mytics
set output "plot.total.eps"
set border linewidth 3
set label 1 "Confidence Interval (CI) 90%" at 300,33000.0
set xrange [0:15200]
set yrange [0:36300]
set xtics 0,200,15200 rotate by 45 offset -0.8,-2
set ytics auto
plot '02_TOTAL.MOTA.4.15000.txt' using 1:2:3:4 with linespoint ls 1 title "Our Algorithm", '02_TOTAL.MOTA.4.15000.txt' using 1:2:3:4 with errorbars ls 1 notitle,'02_TOTAL.C1G2.4.15000.txt' using 1:2:3:4 with linespoint ls 2 title "C1G2", '02_TOTAL.C1G2.4.15000.txt' using 1:2:3:4 with errorbars ls 2 notitle,'02_TOTAL.LOWER.128.15000.txt' using 1:2:3:4 with linespoint ls 3 title "LOWER", '02_TOTAL.LOWER.128.15000.txt' using 1:2:3:4 with errorbars ls 3 notitle,'02_TOTAL.SCHOUTE.128.15000.txt' using 1:2:3:4 with linespoint ls 4 title "SCHOUTE", '02_TOTAL.SCHOUTE.128.15000.txt' using 1:2:3:4 with errorbars ls 4 notitle,'02_TOTAL.NEDFSA.4.15000.txt' using 1:2:3:4 with linespoint ls 6 title "NEDFSA", '02_TOTAL.NEDFSA.4.15000.txt' using 1:2:3:4 with errorbars ls 6 notitle,'02_TOTAL.EOMLEE.128.15000.txt' using 1:2:3:4 with linespoint ls 5 title "EOMLEE", '02_TOTAL.EOMLEE.128.15000.txt' using 1:2:3:4 with errorbars ls 5 notitle
